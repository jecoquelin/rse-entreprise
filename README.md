# rse-entreprise

Ce dépôt contient des documents sur une RSE de la SNCF.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Cette sae nous sensibilisait sur le sujet de l'informatique et de l'environnement. Ainsi, nous avons, en équipe, choisi une entreprise et faire une critique sur les engagements qu'elle prenait pour préserver l'environnement.

Cette sae s'est déroulé en 5 étapes :

1. Il a fallu tout d'abord choisir l'organisation en groupe. Pour notre groupe, cette organisation était la SNCF.
2. Ensuite, nous avons réalisé la fiche signalétique de cette organisation.
3. Puis nous avons fait la matrice swot avec les forces/faiblesses, opportunité/menaces.
4. Nous avons ensuite réalisé le diagnostique interne de l'entreprise puis le diagnostic externe.
5. En dernier lieu, nous avons fait une soutenance orale.

## Apprentissage critique

AC 16.01 : Appréhender l'écosystème numérique